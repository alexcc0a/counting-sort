package com.nesterov;

public class CountingSort {
    public static void countingSort(int[] array) {
        int n = array.length;

        // Находим максимальное значение в массиве.
        int max = array[0];

        for (int i = 1; i < n; i++) {
            if (array[i] > max) {
                max = array[i];
            }
        }

        // Создаём массив для подсчёта количества элементов.
        int[] count = new int[max + 1];

        // Подсчитываем количество каждого элемента.
        for (int i = 0; i < n; i++) {
            count[array[i]]++;
        }

        // Вычисляем прифексные сыммы в массиве count.
        for (int i = 1; i <= max; i++) {
            count[i] += count[i - 1];
        }

        // Создаём временный массив для сохранения отсортированных элементов.
        int[] temp = new int[n];

        // Сортируем элементы в массиве array.
        for (int i = n - 1; i >= 0; i--) {
            temp[count[array[i]] - 1] = array[i];
            count[array[i]]--;
        }

        // Копируем отсортированные элементы из временного массива в исходный массив.
        for (int i = 0; i < n; i++) {
            array[i] = temp[i];
        }
    }

    public static void main(String[] args) {
        // Создаём исходный массив.
        int[] array = {5, 2, 8, 1, 9, 3};

        // Вызываем метод сортировки посчётом.
        countingSort(array);

        // Выводим отсортированный массив.
        for (int num : array) {
            System.out.println(num + " ");
        }
    }
}
